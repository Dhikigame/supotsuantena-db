<?php

require_once 'DSN.php';//DSN接続（DBサーバーに接続）

function db_create_matome_antenna($tbl_name){
	$link = MYSQL_connect();
	DB_select($link);

	$CREATE_MATOME = 
		"create table ".$tbl_name."(
		article_TITLE varchar(255),
		article_URL varchar(200),
		article_IMG varchar(200),
		create_DATE DATETIME,
		regist_DATE DATETIME,
		mainmatome_URL varchar(200),
		mainmatome_name varchar(200),
		main_tag varchar(200),
		sub_tag1 varchar(200),
		sub_tag2 varchar(200),
		sub_tag3 varchar(200),
		sub_tag4 varchar(200),
		sub_tag5 varchar(200),
		sub_tag6 varchar(200),
		sub_tag7 varchar(200),
		sub_tag8 varchar(200),
		sub_tag9 varchar(200),
		sub_tag10 varchar(200),
		sub_tag11 varchar(200),
		sub_tag12 varchar(200)
		)";

	$query = mysqli_query($link, $CREATE_MATOME);//SQLのクエリ送信（クエリ：DBに情報要求）
	//クエリ取得できないならエラー
	if (!$query){
		die("クエリ送信失敗<br />SQL:".$CREATE_MATOME);
	}

	MYSQL_close($link);
}
?>