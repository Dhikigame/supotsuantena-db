# スポーツアンテナ！(DB)

5chまとめサイトや個人スポーツ関連のブログを登録したり、最新記事を更新するためのスクリプト群

Scripts for registering 5ch summary sites and blogs related to individual sports, and updating the latest articles

# Requirement

- PHP7
- MySQL
- Mecab

  Operating environment

- さくらVPS
- CentOS7(DB Server)

# Features

- MySQLにサイトや記事を登録するためにMecabで形態素解析して記事に合うタグを登録(サッカーや野球など)

# Author

- Dhiki(Infrastructure engineer & Video contributor)
- https://twitter.com/DHIKI_pico