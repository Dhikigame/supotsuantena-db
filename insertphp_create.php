<h3>ファイル作成と書き込み読み込み</h3>
<?php
//MATOME_ANTENNAデータベースにブログのテーブル追加
require_once './db_create.php';
//CATEGORY_SPORTSテーブルにブログの情報を登録
require_once './db_insert_CATEGORYSPORTS.php';
//MATOME_ANTENNAの生成したテーブルにinsertする
require_once './insertphp_template.php';

//情報入力
/*  
    $title:ブログタイトル
    $main_site_URL:メインサイトのURL
    $RSSver:1.0または2.0バージョンのどちらか選択
    $category:スポーツのカテゴリ値
    $RSS_URL:RSSのURL
    $table_name:DBで使用するテーブル名
*/
//  *****   記入    *****   //
$title = "Quickスポーツまとめ";
$main_site_URL = "https://quicksportsmatome.blog.jp/";
$RSSver = 1;
$category = 2;
$RSS_URL = "https://quicksportsmatome.blog.jp/index.rdf";
$table_name = "quicksportsmatome";

$sport = "サッカー";
$team = "野球";
//  *****   記入    *****   //

//MATOME_ANTENNAデータベースにブログのテーブル追加
db_create_matome_antenna($table_name);
//CATEGORY_SPORTSテーブルにブログの情報を登録
insert_categorysports($table_name, $sport, $team);

//ファイル名
$sFileName = $table_name.'.php';
//ファイルパス
$sPath = './insert/'.$sFileName;

//書き込む内容
$sWriteContents =    "<?php\n";
$sWriteContents .=   insert_rss($title, $main_site_URL, $RSSver, $category, $RSS_URL, $table_name, $sport, $team);
$sWriteContents .=   "?>";
 
//ファイルの存在確認
if(file_exists($sPath)){
    echo '・指定ファイルが既に存在しております。';
    exit;
}else{
    echo '・ファイルの存在確認完了。<br/>';
}
 
//ファイルを作成
if(touch($sPath)){
    echo '・ファイル作成完了。<br/>';
}else{
    echo '・ファイル作成失敗。<br/>';
    exit;
}
 
//ファイルのパーティションの変更
if(chmod($sPath,0644)){
    echo '・ファイルパーミッション変更完了。<br/>';
}else{
    echo '・ファイルパーミッション変更失敗。<br/>';
    exit;
}
 
//ファイルをオープン
if($filepoint = fopen($sPath,"w")){
    echo '・ファイルオープン完了。<br/>';
}else{
    echo '・ファイルオープン失敗。<br/>';
    exit;
}
 
//ファイルのロック
if(flock($filepoint, LOCK_EX)){
    echo '・ファイルロック完了。<br/>';
}else{
    echo '・ファイルロック失敗。<br/>';
    exit;
}
 
//ファイルへ書き込み
if(fwrite($filepoint,$sWriteContents)){
    echo '・ファイル書き込み完了。<br/>';
}else{
    echo '・ファイル書き込み失敗。<br/>';
    exit;
}
 
//ファイルのアンロック
if(flock($filepoint, LOCK_UN)){
    echo '・ファイルアンロック完了。<br/>';
}else{
    echo '・ファイルアンロック失敗。<br/>';
    exit;
}
 
//ファイルを読み込む
if(is_readable($sPath)){
 
    //ファイル内容を変数に格納
    $vContents = file_get_contents($sPath);
    echo '・ファイル ['.$sFileName.'] 読み込み完了。<br/>';
 
    //ファイル内容を配列に変換 行を取得
    $aCcontents = explode("\n", $vContents);
 
    //読み込み内容を展開
    echo '<table border="1">';
    foreach($aCcontents as $aContentsRecords){
        echo '<tr>';
        $aContentsFields = explode("\t", $aContentsRecords);
        //ファイル内容を配列に変換 列を取得
        foreach($aContentsFields as $data){
            echo "<td>{$data}</td>";
        }
        echo '</tr>';
    }
    echo '</table>';
 
}else{
    echo '・ファイル ['.$sFileName.'] 読み込み失敗。<br/>';
    exit;
}
 
//ファイルを閉じる
if(fclose($filepoint)){
    echo '・ファイルクローズ完了。<br/>';
}else{
    echo '・ファイルクローズ失敗。<br/>';
    exit;
}

//MATOME_ANTENNAの生成したテーブルに記事をinsertする
//require_once './insertphp_article.php';

?>