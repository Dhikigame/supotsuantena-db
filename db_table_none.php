<?php
/***
		テーブルカラム変更用プログラム
***/

require_once 'DSN.php';//DSN接続（DBサーバーに接続）
$link = MYSQL_connect();
$db = DB_select($link);

//テーブル全取得
$all_tbl = "SHOW TABLES FROM MATOME_ANTENNA";

$result_tbl= mysqli_query($link, $all_tbl);//SQLのクエリ送信（クエリ：DBに情報要求）
if (!$result_tbl){//クエリ取得できないならエラー
	die("クエリ送信失敗<br />SQL:".$all_tbl);
}

$rows_tbl = mysqli_num_rows($result_tbl);//SQLの結果の行数を取得

if($rows_tbl){//SQLの結果あるなら出力
	$i = 0;
    while($tbl = mysqli_fetch_array($result_tbl)) {
      //echo $tbl[0]."<br>";
      $tbl_name[$i] = $tbl[0];
      $i++;
    }
    $j = $i;
}

$i = 0;

//$tbl_name = "nanjstadium";
while($j > $i){
	$alter_table = "TRUNCATE ".$tbl_name[$i]."";

	$query = mysqli_query($link, $alter_table);//SQLのクエリ送信（クエリ：DBに情報要求）
	//クエリ取得できないならエラー
	if (!$query){
		print("クエリ送信失敗 SQL:".$alter_table."<br />");
	}
	$i++;
}



?>

<?php
MYSQL_close($link);
?>