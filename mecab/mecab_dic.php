<?php

    /*
        使うMecabの辞書を$optionsに登録する
        あらかじめ、php内でMecab使える設定と使う辞書のパス指定の必要がある(使用したMecabの辞書　GitHub:https://github.com/neologd/mecab-ipadic-neologd)
    */
function mecab_dic(){
    
	//DBサーバか開発PCかIPアドレスから調べ、使うMecabの辞書を$optionに登録する
	$IPADDR = isset($_SERVER['SERVER_ADDR'])?$_SERVER['SERVER_ADDR']:gethostbyname(gethostname());
	if($IPADDR == '160.16.217.168' || $IPADDR == '192.168.0.1' || $IPADDR == '160.16.226.147' || $IPADDR == '192.168.0.2'){
		$options = ['-d', '/usr/lib64/mecab/dic/mecab-ipadic-neologd'];
	}else if($IPADDR == '192.168.0.8' || strtoupper(substr(PHP_OS, 0, 6)) === 'LINUX') {
		//echo strtoupper(substr(PHP_OS, 0, 6));
    	$options = ['-d', '/usr/lib/x86_64-linux-gnu/mecab/dic/mecab-ipadic-neologd/'];
	}else{
		$options = ['-d', '/usr/local/mecab/lib/mecab/dic/mecab-ipadic-neologd/'];
    }
    
    return $options;
}

?>