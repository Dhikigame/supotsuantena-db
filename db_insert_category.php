<?php
/***
		カテゴリテーブルデータ変更用プログラム(db_insert.phpが実行したら常に最新のデータに更新しておく)
***/

require_once 'DSN.php';//DSN接続（DBサーバーに接続）
$link = MYSQL_connect();
$db = DB_select_category($link);

//テーブル全取得
$all_tbl = "select table_name from CATEGORY_SPORTS";

$result_tbl= mysqli_query($link, $all_tbl);//SQLのクエリ送信（クエリ：DBに情報要求）
if (!$result_tbl){//クエリ取得できないならエラー
	die("クエリ送信失敗<br />SQL1:".$all_tbl);
}

$rows_tbl = mysqli_num_rows($result_tbl);//SQLの結果の行数を取得
//echo $rows_tbl;

if($rows_tbl){//SQLの結果あるなら出力
    while($tbl = mysqli_fetch_array($result_tbl)) {
      //echo $tbl[0]."<br>";
	  //echo $tbl_name[0]."<br>";
		/***	rss_DATE更新		***/
		$db = DB_select($link);	
		$old_regist_DATE_SQL = "select MIN(regist_DATE) from ".$tbl[0];
		//echo $old_regist_DATE_SQL;
		$old_regist_DATE_query = mysqli_query($link, $old_regist_DATE_SQL);//SQLのクエリ送信（クエリ：DBに情報要求）
		//クエリ取得できないならエラー
		if (!$old_regist_DATE_query){
			print("クエリ送信失敗 SQL2:".$old_regist_DATE_SQL."<br />");
		}
		$rows_tbl = mysqli_num_rows($old_regist_DATE_query);//SQLの結果の行数を取得

		if($rows_tbl){//SQLの結果あるなら出力
			$tbl2 = mysqli_fetch_array($old_regist_DATE_query);
			$old_regist_DATE = $tbl2[0];
		}
		$db = DB_select_category($link);

		$insert_tbl = "update CATEGORY_MATOME.CATEGORY_SPORTS set rss_DATE = '".$old_regist_DATE."' where table_name = '".$tbl[0]."'";
		$query = mysqli_query($link, $insert_tbl);//SQLのクエリ送信（クエリ：DBに情報要求）
		//クエリ取得できないならエラー
		if (!$query){
			print("クエリ送信失敗 SQL3:".$insert_tbl."<br />");
		}



		/***	regist_DATE更新		***/
		$db = DB_select($link);
		$new_regist_DATE_SQL = "select MAX(regist_DATE) from ".$tbl[0];

		$new_regist_DATE_query = mysqli_query($link, $new_regist_DATE_SQL);//SQLのクエリ送信（クエリ：DBに情報要求）
		//クエリ取得できないならエラー
		if (!$new_regist_DATE_query){
			print("クエリ送信失敗 SQL2:".$new_regist_DATE_SQL."<br />");
		}
		$rows_tbl = mysqli_num_rows($new_regist_DATE_query);//SQLの結果の行数を取得
		
		if($rows_tbl){//SQLの結果あるなら出力
			$tbl2 = mysqli_fetch_array($new_regist_DATE_query);
			$new_regist_DATE = $tbl2[0];
		}
		$db = DB_select_category($link);

		$insert_tbl = "update CATEGORY_MATOME.CATEGORY_SPORTS set regist_DATE = '".$new_regist_DATE."' where table_name = '".$tbl[0]."'";
		$query = mysqli_query($link, $insert_tbl);//SQLのクエリ送信（クエリ：DBに情報要求）
		//クエリ取得できないならエラー
		if (!$query){
			print("クエリ送信失敗 SQL3:".$insert_tbl."<br />");
		}



		/***	mainmatome_name更新	***/
		$db = DB_select($link);
		$new_regist_DATE_SQL = "select mainmatome_name from ".$tbl[0]." where regist_DATE = (select MAX(regist_DATE) from ".$tbl[0].")";

		$new_regist_DATE_query = mysqli_query($link, $new_regist_DATE_SQL);//SQLのクエリ送信（クエリ：DBに情報要求）
		//クエリ取得できないならエラー
		if (!$new_regist_DATE_query){
			print("クエリ送信失敗 SQL2:".$new_regist_DATE_SQL."<br />");
		}
		$rows_tbl = mysqli_num_rows($new_regist_DATE_query);//SQLの結果の行数を取得
		
		if($rows_tbl){//SQLの結果あるなら出力
			$tbl2 = mysqli_fetch_array($new_regist_DATE_query);
			$new_regist_DATE = $tbl2[0];
		}
		$db = DB_select_category($link);

		$insert_tbl = "update CATEGORY_MATOME.CATEGORY_SPORTS set mainmatome_name = '".$new_regist_DATE."' where table_name = '".$tbl[0]."'";
		$query = mysqli_query($link, $insert_tbl);//SQLのクエリ送信（クエリ：DBに情報要求）
		//クエリ取得できないならエラー
		if (!$query){
			print("クエリ送信失敗 SQL3:".$insert_tbl."<br />");
		}


		/***	mainmatome_URL更新	***/
		$db = DB_select($link);
		$new_regist_DATE_SQL = "select mainmatome_URL from ".$tbl[0]." where regist_DATE = (select MIN(regist_DATE) from ".$tbl[0].")";

		$new_regist_DATE_query = mysqli_query($link, $new_regist_DATE_SQL);//SQLのクエリ送信（クエリ：DBに情報要求）
		//クエリ取得できないならエラー
		if (!$new_regist_DATE_query){
			print("クエリ送信失敗 SQL2:".$new_regist_DATE_SQL."<br />");
		}
		$rows_tbl = mysqli_num_rows($new_regist_DATE_query);//SQLの結果の行数を取得
		
		if($rows_tbl){//SQLの結果あるなら出力
			$tbl2 = mysqli_fetch_array($new_regist_DATE_query);
			$new_regist_DATE = $tbl2[0];
		}
		$db = DB_select_category($link);

		$insert_tbl = "update CATEGORY_MATOME.CATEGORY_SPORTS set mainmatome_URL = '".$new_regist_DATE."' where table_name = '".$tbl[0]."'";
		$query = mysqli_query($link, $insert_tbl);//SQLのクエリ送信（クエリ：DBに情報要求）
		//クエリ取得できないならエラー
		if (!$query){
			print("クエリ送信失敗 SQL3:".$insert_tbl."<br />");
		}
      //$i++;
    }
    //$j = $i;
}
?>