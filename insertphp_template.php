<?php

function insert_rss($title, $main_site_URL, $RSSver, $category, $RSS_URL, $table_name, $sport, $team){
    $sWriteContents = "/*****     ".$sport."      *****/ \n"; 
    $sWriteContents .= "/***     ".$team."      ***/ \n"; 
    $sWriteContents .= "/*     ".$title."      */ \n"; 
    $sWriteContents .= "require_once 'DSN.php';//DSN接続（DBサーバーに接続）\n";
    $sWriteContents .= "require_once 'date_update.php';//更新時刻アップデート \n";
    $sWriteContents .= "$"."link = MYSQL_connect(); \n";
    $sWriteContents .= "DB_select("."$"."link); \n";
    $sWriteContents .= "require_once 'rss_parse.php';//RSS1.0情報取得 \n";
    $sWriteContents .= "require_once 'rss_parse2.php';//RSS2.0情報取得 \n";

    if($RSSver === 1){
        $sWriteContents .= "RSS_parse('".$RSS_URL."', '".$main_site_URL."', "."$"."link, '".$table_name."', 'MATOME_ANTENNA', ".$category."); \n";
    }else{
        $sWriteContents .= "RSS_parse2('".$RSS_URL."', '".$main_site_URL."', "."$"."link, '".$table_name."', 'MATOME_ANTENNA', ".$category."); \n";
    }

    $sWriteContents .= "require_once 'db_insert_category.php';//カテゴリテーブルデータ変更(db_insertが実行したら常に最新のデータに更新しておく) \n";
    $sWriteContents .= "MYSQL_close("."$"."link); \n";

    return $sWriteContents;
}
?>