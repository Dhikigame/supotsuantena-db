<?php
/***
		RSS parse バージョンRSS2.0版
***/
function RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, $db_name, $sports_num){
	$xml = simplexml_load_file($RSS_URL, 'SimpleXMLElement', LIBXML_NOCDATA);
	$matomesite_title = $xml->channel->title;
	//echo $matomesite_title;
	foreach($xml->channel->item as $entry){
		
		//記事タイトル
		$site_title = $entry->title;
		echo $site_title;
		//サイトリンク
		$site_link = $entry->link;

		//contentタグからimgタグ取得
		$description = (string)$entry->description;
		preg_match('/<img.*>/i', $description, $entryimg);
		$searchPattern = '/<img.*?src=(["\'])(.+?)\1.*?>/i';
		preg_match($searchPattern, $entryimg[0], $imgurl);
		//$parse[1] = '<img src="'.$imgurl[2].'" width="193" height="130" border="0">';
		//echo "画像：".$parse[2];
		//記事作成日付
		$date = date('Y-m-d H:i:s',strtotime($entry->pubDate));
		if($date == "1970-01-01 09:00:00"){
			$date = date('Y-m-d H:i:s',strtotime($entry->children('dc',true)->date));
		}
		//メインタグ
		$subject = (string)$entry->category;
		echo $subject."<br>";

		//重複した記事をDB登録しない
		$db_dupl = "SELECT * FROM ".$tbl_name." WHERE article_URL IN('".$site_link."')";
		$query = mysqli_query($link, $db_dupl);//SQLのクエリ送信（クエリ：DBに情報要求）
		echo "dupl";

		//タイトル・メインタグ形態素解析によるsub_tag登録
		require_once 'word_analysis.php';
		$tags = sub_tag_analysis($tbl_name, $site_title, $subject, $sports_num);

		//クエリ取得できないならエラー
		if (!$query){
			print("クエリ送信失敗<br />SQL:".$db_dupl);
		}
		$rows_player = mysqli_num_rows($query);//重複した記事判定取得[0,1]
		echo $rows_player;

		//重複した記事なければ記事をDB登録
		if(!$rows_player){
			//現在日付
			$current_date = date("Y-m-d H:i:s");
			$db_insert = "insert into MATOME_ANTENNA.".$tbl_name."(article_TITLE, article_URL, article_IMG, create_DATE, regist_DATE, mainmatome_URL, mainmatome_name, main_tag, sub_tag1, sub_tag2, sub_tag3, sub_tag4, sub_tag5, sub_tag6, sub_tag7, sub_tag8, sub_tag9, sub_tag10, sub_tag11, sub_tag12) values('".$site_title."', '".$site_link."', '".$imgurl[2]."', '".$date."', '".$current_date."', '".$MAIN_URL."', '".$matomesite_title."', '".$subject."', '".$tags[0]."', '".$tags[1]."', '".$tags[2]."', '".$tags[3]."', '".$tags[4]."', '".$tags[5]."', '".$tags[6]."', '".$tags[7]."', '".$tags[8]."', '".$tags[9]."', '".$tags[10]."', '".$tags[11]."')";

			$query = mysqli_query($link, $db_insert);//SQLのクエリ送信（クエリ：DBに情報要求）
			echo "string";
			//クエリ取得できないならエラー
			if (!$query){
				print("クエリ送信失敗<br />SQL:".$db_insert);
			}
		}
	}
}
?>