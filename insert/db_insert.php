<?php

require_once 'DSN.php';//DSN接続（DBサーバーに接続）
require_once 'date_update.php';//更新時刻アップデート
$link = MYSQL_connect();
$db = DB_select($link);
require_once 'rss_parse.php';//RSS1.0情報取得
require_once 'rss_parse2.php';//RSS2.0情報取得

?>

<?php
/*****

	野球(sports_num = 1)

*****/
/****
	プロ野球
****/
/***	総合		***/

/** なんJスタジアム **/
$tbl_name = "nanjstadium";
$RSS_URL = "http://blog.livedoor.jp/nanjstu/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/nanjstu/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/** 日刊やきう速報＠野球まとめ **/
$tbl_name = "nikkanyakyusokuhou";
$RSS_URL = "http://blog.livedoor.jp/yakiusoku/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/yakiusoku/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/** なんJ PRIDE **/
$tbl_name = "nanjpride";
$RSS_URL = "http://blog.livedoor.jp/rock1963roll/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/rock1963roll/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/** なんJ（まとめては）いかんのか？ **/
$tbl_name = "nanjmatometehaikannoka";
$RSS_URL = "http://blog.livedoor.jp/livejupiter2/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/livejupiter2/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/** 非常識＠なんJ～野球ネタを中心にまとめるブログ～ **/
$tbl_name = "hizyoushikinanj";
$RSS_URL = "http://absurd.blogo.jp/index.rdf";
$MAIN_URL = "http://absurd.blogo.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	MLB
****/
/***	総合		***/

/** MLB NEWS@なんJ **/
$tbl_name = "mlb_news";
$RSS_URL = "http://blog.livedoor.jp/i6469/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/i6469/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/** MLB Note **/
$tbl_name = "mlb_note";
$RSS_URL = "http://mlbmatome.blog.jp/index.rdf";
$MAIN_URL = "http://mlbmatome.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	巨人		***/

/** Ｇ速@読売ジャイアンツまとめブログ **/
$tbl_name = "g_soku";
$RSS_URL = "http://g-soku.blog.jp/index.rdf";
$MAIN_URL = "http://g-soku.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/** 永久不滅ジャイアンツ **/
$tbl_name = "eikyuhumetsugiants";
$RSS_URL = "http://egiants.blog.jp/index.rdf";
$MAIN_URL = "http://egiants.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	中日		***/

/***	どら報@中日ドラゴンズまとめ		***/
$tbl_name = "dorahou";
$RSS_URL = "http://blog.livedoor.jp/doraho/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/doraho/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/***	竜速（りゅうそく）		***/
$tbl_name = "ryuusoku";
$RSS_URL = "http://blog.livedoor.jp/bbyakyu/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/bbyakyu/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	ヤクルト		***/

/***	ツバメ速報＠東京ヤクルトスワローズまとめ		***/
$tbl_name = "tsubamesokuhou";
$RSS_URL = "http://tsubamesoku.blog.jp/index.rdf";
$MAIN_URL = "http://tsubamesoku.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	阪神		***/

/***	阪神タイガースちゃんねる		***/
$tbl_name = "hanshinchannel";
$RSS_URL = "http://hanshintigers1.blog.jp/index.rdf";
$MAIN_URL = "http://hanshintigers1.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/***	虎速		***/
$tbl_name = "torasoku";
$RSS_URL = "http://torasoku.blog.jp/index.rdf";
$MAIN_URL = "http://torasoku.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	広島　　	***/

/***	鯉速＠広島東洋カープまとめブログ		***/
$tbl_name = "koisoku";
$RSS_URL = "http://koisoku.ldblog.jp/index.rdf";
$MAIN_URL = "http://koisoku.ldblog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/***	広島東洋カープまとめブログ | かーぷぶーん		***/
$tbl_name = "carpbun";
$RSS_URL = "http://carp-matome.blog.jp/index.rdf";
$MAIN_URL = "http://carp-matome.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	横浜DeNA　　	***/

/***	De速		***/
$tbl_name = "desoku";
$RSS_URL = "http://de-baystars.doorblog.jp/index.rdf";
$MAIN_URL = "http://de-baystars.doorblog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/***	ベイスターズ速報＠なんJ		***/
$tbl_name = "beisutasokuhou";
$RSS_URL = "http://nanjde.blog.jp/index.rdf";
$MAIN_URL = "http://nanjde.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	ソフトバンク　　	***/

/***	鷹速@ホークスまとめブログ		***/
$tbl_name = "takasoku";
$RSS_URL = "http://blog.livedoor.jp/hawksmatome/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/hawksmatome/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	西武		***/

/***	やみ速@西武まとめ		***/
$tbl_name = "yamisoku";
$RSS_URL = "http://blog.livedoor.jp/pawapuro_kouryaku/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/pawapuro_kouryaku/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/***	野球猫びいき		***/
$tbl_name = "yakyunekobiiki";
$RSS_URL = "http://blog.livedoor.jp/plhfmbe/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/plhfmbe/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	楽天		***/

/***	鷲づかみニュース＠楽天球団専		***/
$tbl_name = "takadukami";
$RSS_URL = "http://wasidukami.club/index.rdf";
$MAIN_URL = "http://wasidukami.club/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/***	狗鷲タイムス		***/
$tbl_name = "inuwashitimes";
$RSS_URL = "http://inuwashitimes.blog.jp/index.rdf";
$MAIN_URL = "http://inuwashitimes.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	ロッテ		***/

/***	まとめロッテ！		***/
$tbl_name = "matomelotte";
$RSS_URL = "http://matomelotte.com/index.rdf";
$MAIN_URL = "http://matomelotte.com/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/***	マリーンズまとめ速！		***/
$tbl_name = "marinesmatomesoku";
$RSS_URL = "http://marines-matomesoku.blog.jp/index.rdf";
$MAIN_URL = "http://marines-matomesoku.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	オリックス		***/

/***	ORIXBLOG＠オリックス・バファローズまとめブログ		***/
$tbl_name = "orixmatomeblog";
$RSS_URL = "http://cowboy.blog.jp/index.rdf";
$MAIN_URL = "http://cowboy.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/***	オリックス速報＠なんJまとめ		***/
$tbl_name = "orixsokuhou";
$RSS_URL = "http://orisoku.doorblog.jp/index.rdf";
$MAIN_URL = "http://orisoku.doorblog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	野球

*****/
/****
	プロ野球
****/
/***	日本ハム		***/

/***	ファイターズ王国＠日ハムまとめブログ		***/
$tbl_name = "fighterskingdom";
$RSS_URL = "http://fighters-kingdom.blog.jp/index.rdf";
$MAIN_URL = "http://fighters-kingdom.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

/***	ポリー速報		***/
$tbl_name = "porysokuhou";
$RSS_URL = "http://blog.livedoor.jp/fightersmatome/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/fightersmatome/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 1);

?>

<?php
/*****

	サッカー(sports_num = 2)

*****/
/***	総合		***/

/** カルチョまとめブログ **/
$tbl_name = "karucchomatomeblog";
$RSS_URL = "http://www.calciomatome.net/index.rdf";
$MAIN_URL = "http://www.calciomatome.net/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);
/** SAMURAI Footballers **/
$tbl_name = "samuraifootbllers";
$RSS_URL = "http://samuraisoccer.doorblog.jp/index.rdf";
$MAIN_URL = "http://samuraisoccer.doorblog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);
/** サカラボ | サッカーまとめ速報 **/
$tbl_name = "sakarabo";
$RSS_URL = "http://sakarabo.blog.jp/index.rdf";
$MAIN_URL = "http://sakarabo.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);
/** サカサカ10【サッカーまとめ速報】 **/
$tbl_name = "sakasaka10";
$RSS_URL = "http://sakasaka10.blog.jp/index.rdf";
$MAIN_URL = "http://sakasaka10.blog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);
/** footballnet【サッカーまとめ】 **/
$tbl_name = "footballnet";
$RSS_URL = "http://footballnet.2chblog.jp/index.rdf";
$MAIN_URL = "http://footballnet.2chblog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);
/** WorldFootballNewS **/
$tbl_name = "worldfn";
$RSS_URL = "http://worldfn.net/index.rdf";
$MAIN_URL = "http://worldfn.net/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);
/** NO FOOTY NO LIFE **/
$tbl_name = "nofootynolife";
$RSS_URL = "http://nofootynolife.blog.fc2.com/?xml";
$MAIN_URL = "http://nofootynolife.blog.fc2.com/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);
/** フットカルチョ **/
$tbl_name = "footcalcio";
$RSS_URL = "http://blog.livedoor.jp/footcalcio/index.rdf";
$MAIN_URL = "http://blog.livedoor.jp/footcalcio/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);
/** Samurai GOAL **/
$tbl_name = "samuraigoal";
$RSS_URL = "http://samuraigoal.doorblog.jp/index.rdf";
$MAIN_URL = "http://samuraigoal.doorblog.jp/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);
/** ドメサカブログ **/
$tbl_name = "domesoccer";
$RSS_URL = "http://blog.domesoccer.jp/feed";
$MAIN_URL = "http://blog.domesoccer.jp";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

?>


<?php

/***	札幌		***/
/** コンサと生きる **/
$tbl_name = "konsatoikiru";
$RSS_URL = "http://www.consadole.net/tnfaki/rss2_0/feed.xml";
$MAIN_URL = "http://www.consadole.net/tnfaki/";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	鹿島		***/
/** 鹿島アントラーズ原理主義 **/
$tbl_name = "kashimagenri";
$RSS_URL = "http://fundamentalism.blog54.fc2.com/?xml";
$MAIN_URL = "http://fundamentalism.blog54.fc2.com/";
$parse = RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	仙台		***/
/** Omoitattara･･･ 　　　　 Sanryutei-Douraku **/
$tbl_name = "douraku3";
$RSS_URL = "http://rssblog.ameba.jp/douraku3/rss20.xml";
$MAIN_URL = "https://ameblo.jp/douraku3/";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	浦和		***/
/** 浦和レッズの逆襲日報 **/
$tbl_name = "redsgyakushuu";
$RSS_URL = "http://redsgyakushuu.blog.shinobi.jp/RSS/200/";
$MAIN_URL = "http://redsgyakushuu.blog.shinobi.jp/";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	柏		***/
/** 目指せアジア王者&J1！柏レイソル&FC岐阜山岸祐也を勝手に応援 **/
$tbl_name = "kashiwaj1";
$RSS_URL = "http://rssblog.ameba.jp/goki0418/rss20.xml";
$MAIN_URL = "https://ameblo.jp/goki0418/";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	FC東京　	***/
/** アオトアカーCurry&BAR（カレー＆バー）ーFC東京を全力で応援するお店のブログー（移転準備中の為、お店は現在クローズしています） **/
$tbl_name = "aotoaka";
$RSS_URL = "http://rssblog.ameba.jp/baraotoaka/rss20.xml";
$MAIN_URL = "https://ameblo.jp/baraotoaka/";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	川崎		***/
/** 青と黒の軌跡 川崎と共に！ **/
$tbl_name = "furonta";
$RSS_URL = "http://rssblog.ameba.jp/furonta/rss20.xml";
$MAIN_URL = "https://ameblo.jp/furonta/";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	横浜マリノス		***/
/** こけまり **/
$tbl_name = "kokemari";
$RSS_URL = "http://kokemari.com/feed/";
$MAIN_URL = "http://kokemari.com";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	湘南		***/
/** 明治安田生命J1リーグに所属する湘南ベルマーレを応援するブログです。毎日更新！ **/
$tbl_name = "kokemari";
$RSS_URL = "https://bellmare.live/feed";
$MAIN_URL = "https://bellmare.live";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	清水		***/
/** エスパルスインフォ **/			//parseできてない
$tbl_name = "spulse";
$RSS_URL = "https://www.spulse.info/feed";
$MAIN_URL = "https://www.spulse.info";
$parse = RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	磐田		***/
/** ジュビロ磐田応援隊 **/
$tbl_name = "jubilo_ouen";
$RSS_URL = "http://rssblog.ameba.jp/tanappy0520/rss20.xml";
$MAIN_URL = "https://ameblo.jp/tanappy0520/";
RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	名古屋		***/
/** グランパス定点観測記 **/
$tbl_name = "grampuskansoku";
$RSS_URL = "https://pixy7750.blog.so-net.ne.jp/index.rdf";
$MAIN_URL = "https://pixy7750.blog.so-net.ne.jp/";
RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	G大坂		***/
/** GAMBAKA blog ~あの焼く音 あの味~ **/
$tbl_name = "gambakablog";
$RSS_URL = "http://gambaka9244.blog.fc2.com/?xml";
$MAIN_URL = "http://gambaka9244.blog.fc2.com/";
RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	C大坂		***/
/** VIVA セレッソ大阪 **/
$tbl_name = "vivacerezo";
$RSS_URL = "https://blog.goo.ne.jp/yoshi08/rss2.xml";
$MAIN_URL = "https://blog.goo.ne.jp/yoshi08?fm=rss";
RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	神戸		***/
/** トモニ行くぜ！ヴィッセル神戸！ **/
$tbl_name = "visselkobe";
$RSS_URL = "http://vissel950.blog.fc2.com/?xml";
$MAIN_URL = "http://vissel950.blog.fc2.com/";
RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	広島		***/
/** くのサンフレッチェ・ライフ **/
$tbl_name = "sanfreccelife";
$RSS_URL = "http://slashandburn.cocolog-nifty.com/blog/index.rdf";
$MAIN_URL = "http://slashandburn.cocolog-nifty.com/blog/";
RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	鳥栖		***/
/** いつも心は前向きに！　～サガン鳥栖応援日記～ **/
$tbl_name = "maemukisagantosu";
$RSS_URL = "http://maemukisagantosu.blog.fc2.com/?xml";
$MAIN_URL = "http://maemukisagantosu.blog.fc2.com/";
RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

/***	長崎		***/
/** 長崎から・・・！NA.Hiroのブログ **/
$tbl_name = "nagasakikara";
$RSS_URL = "https://blogs.yahoo.co.jp/prefna1991/rss.xml";
$MAIN_URL = "https://blogs.yahoo.co.jp/prefna1991";
RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 2);

?>



<?php
/*****

	バスケ(sports_num = 3)

*****/
/***	総合		***/

/** バスケまとめ・COM **/
$tbl_name = "basukematome";
$RSS_URL = "http://basketballbbs.com/feed/";
$MAIN_URL = "http://basketballbbs.com";
RSS_parse2($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 3);

/** NBAまとめ！ **/
$tbl_name = "nbamatome";
$RSS_URL = "http://nbama.blog.fc2.com/?xml";
$MAIN_URL = "http://nbama.blog.fc2.com/";
RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 3);

/** んば速報！ **/
$tbl_name = "nbasokuhou";
$RSS_URL = "http://nbamatome.2chblog.jp/index.rdf";
$MAIN_URL = "http://nbamatome.2chblog.jp/";
RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 3);

/** んば速報！ **/
$tbl_name = "nbamatomenba";
$RSS_URL = "http://matomenba.com/?xml";
$MAIN_URL = "http://matomenba.com/";
RSS_parse($RSS_URL, $MAIN_URL, $link, $tbl_name, 'MATOME_ANTENNA', 3);




?>






<?php
require_once 'db_insert_category.php';//カテゴリテーブルデータ変更(db_insertが実行したら常に最新のデータに更新しておく)
MYSQL_close($link);
?>