<?php

require_once 'sub_tag_search.php';

?>

<?php
//test code
$IPADDR = isset($_SERVER['SERVER_ADDR'])?$_SERVER['SERVER_ADDR']:gethostbyname(gethostname());
	if($IPADDR == '160.16.226.147'){
		$options = ['-d', '/usr/lib64/mecab/dic/mecab-ipadic-neologd/'];
	}else{
		$options = ['-d', '/usr/local/mecab/lib/mecab/dic/mecab-ipadic-neologd/'];
	}

$query = "セレッソ戦引き分け";
$sports_num = 2;
$mecab = new \Mecab\Tagger($options);
//メインタグ解析
$nodes = $mecab->parseToNode($query);
$word_count = 0;
$before_word = "";
	foreach($nodes as $node){
		$word = $node->getSurface(); #形態素の表示
		$word_feature = $node->getFeature(); #解析結果の表示
		echo "getSurface:".$word . "<br>";
		echo "getFeature:".$word_feature . "<br>";
		$word_tag = sub_tag_search($word, $before_word, $sports_num);//$search_info[0-1] 0:returnして返ったタグ、1:前のタグ情報格納
		$before_word = $word_tag[0];
		$tags[$word_count] = $word_tag[1];
		//重複していればタグを取り除く
		$count = $word_count - 1;
		while($count >= 0){
			if($tags[$word_count] == $tags[$count]){
				$tags[$word_count] = "None";
				break;
			}
			$count--;
		}

		if($tags[$word_count] != "None"){
			$word_count++;
		}
	}

$i = 0;
echo "最終結果：";
while($word_count >= $i){
	echo $tags[$i] . " ";
	$i++;
}

?>